import st3m.run
from apps.pollin4te.main import Pollin4teApp
from st3m.application import ApplicationContext
st3m.run.run_view(Pollin4teApp(ApplicationContext()))
