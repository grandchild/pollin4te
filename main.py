import json
import math
import os
import select
import socket
import struct
import time

import badgelink
import badgenet
from ctx import Context
from st3m import logging
from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from st3m.goose import Generator, Iterator
from sys_kernel import firmware_version

from st3m.ui.colours import GO_GREEN

# from .files import Files


log = logging.Log(__name__, level=logging.INFO)
log.info("import")

FILE_TX_CHUNK_SIZE = 1024
INVENTORY_TX_MAX_SIZE = 4096

STOCK_APPS_V100 = {
    "clouds",
    "demo_cap_touch",
    "demo_harmonic",
    "demo_imu",
    "demo_melodic",
    "demo_scroll",
    "demo_worms",
    "fil3s",
    "gay_drums",
    "led_painter",
    "nick",
    "otamatone",
    "pollin4te",
    "shoegaze",
    "tiny_sampler",
}
STOCK_APPS = {
    "v1.0.0": STOCK_APPS_V100,
    "v1.1.0": STOCK_APPS_V100.union(
        {
            "gr33nhouse",
        }
    ),
}

FTYPE_DIR = 0x4000
FTYPE_FILE = 0x8000


class DesyncError(Exception):
    pass


class File:
    """A file & dir class with some safety precautions against malicious paths."""

    APPS_DIR = "/flash/sys/apps/"

    def __init__(
        self, for_app: str, path: str, data: bytes | None = None, is_dir: bool = False
    ):
        self.for_app = for_app
        if is_dir and data:
            raise ValueError("Directories cannot have data")
        self.is_dir = is_dir
        if "../" in path:
            raise ValueError(
                f"Not allowed to write outside of '{File.APPS_DIR}{for_app}'. ('{path}')"
            )
        self.path = path
        self.data = data
        if self.is_dir:
            self.data_len = 0
        else:
            if self.data is None:
                try:
                    size = os.stat(self.abspath())[6]
                except OSError:
                    raise ValueError(f"File {self.abspath()} not found")
                self.data_len = size
            else:
                self.data_len = len(self.data)

    def abspath(self):
        return f"{File.APPS_DIR}/{self.for_app}/{self.path}"

    def write(self):
        if self.is_dir:
            raise NotImplemented("Use 'Dir' instance for write() support.")
        try:
            with open(self.abspath(), "wb") as f:
                f.write(self.data or b"")
        except OSError as err:
            log.warning(f"File {self.abspath()} cannot be written: {err}")

    def read(self) -> Iterator[bytes]:
        if self.is_dir:
            raise NotImplemented("Use 'Dir' instance for read() support.")
        if self.data is not None:
            for i in range(0, len(self.data), FILE_TX_CHUNK_SIZE):
                yield self.data[i : i + FILE_TX_CHUNK_SIZE]
        else:
            with open(self.abspath(), "rb") as f:
                while data := f.read(FILE_TX_CHUNK_SIZE):
                    yield data

    def info(self) -> dict[str, bool | str | int]:
        return {
            "is_dir": self.is_dir,
            "path": self.path,
            "length": self.data_len,
        }


class Dir(File):
    def __init__(self, for_app: str, path: str):
        super().__init__(for_app, path, is_dir=True)

    def write(self):
        try:
            os.mkdir(self.abspath())
        except OSError:
            log.warning(
                f"Directory {self.abspath()} cannot be created (already exists?)"
            )

    def read(self) -> Iterator[bytes]:
        return  # type: ignore


class AppPackage:
    def __init__(self, app_name: str, files: list[File | Dir] | None = None):
        self.app_name = app_name
        self._invalid = False
        if files:
            self.files = files
        else:
            self.files = sorted(
                self._collect_app_files(), key=lambda f: (not f.is_dir, f.path)
            )

    def _collect_app_files(self, current="") -> list[File | Dir]:
        files = []
        cur_dir = f"{File.APPS_DIR}/{self.app_name}/{current}"
        try:
            listing = os.ilistdir(cur_dir)
        except OSError:
            log.error(f"Directory {cur_dir} not found or not readable")
            self._invalid = True
            return []
        for name, ftype, *extra in listing:
            if name == "__pycache__":
                continue
            path = f"{current}/{name}"
            if ftype == FTYPE_DIR:
                files.append(Dir(self.app_name, path))
                files += self._collect_app_files(f"{current}/{name}")
            elif ftype == FTYPE_FILE:
                files.append(File(self.app_name, path))
            else:
                raise ValueError(f"Unknown file type: {ftype}")
        return files

    def to_json(self) -> str:
        return json.dumps(
            {
                "name": self.app_name,
                "files": [file.info() for file in self.files],
            }
        )

    @staticmethod
    def from_json(data: dict) -> "AppPackage":
        return AppPackage(
            data["name"],
            [
                File(data["name"], file["path"], None, is_dir=file["is_dir"])
                for file in data["files"]
            ],
        )


class Configuration:
    def __init__(self) -> None:
        self.is_sender = True

    @classmethod
    def load(cls, path: str) -> "Configuration":
        res = cls()
        try:
            with open(path) as f:
                jsondata = f.read()
            data = json.loads(jsondata)
        except OSError:
            data = {}
        if "is_sender" in data and isinstance(data["is_sender"], bool):
            res.is_sender = data["is_sender"]
        return res


class Pollin4teApp(Application):
    DISCOVER_PORT = 9011
    DATA_PORT = 9012
    DISCOVER_BEACON_LEN = 32

    PURPLE = (1, 0, 1)

    def __init__(self, app_ctx: ApplicationContext, *args) -> None:
        super().__init__(app_ctx)
        self._time = 0
        self._config = Configuration.load("/flash/pollin4te.json")
        self._local_addr = "ff02::1%" + badgenet.get_interface().name()
        self._peer: tuple[str, tuple[int, int, int]] | None = None
        if self._config.is_sender:
            badgenet.configure_jack(badgenet.SIDE_RIGHT, badgenet.MODE_ENABLE_AUTO)
            badgelink.right.enable()
            # write_test_app()
        else:
            badgenet.configure_jack(badgenet.SIDE_LEFT, badgenet.MODE_ENABLE_AUTO)
            badgelink.left.enable()
        self._discover_poll = select.poll()
        self._discover_sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
        self._discover_sock.bind((self._local_addr, self.DISCOVER_PORT))
        self._discover_poll.register(self._discover_sock, select.POLLIN)

        receive_addr = f"{badgenet.get_interface().ifconfig6()[0]}%{badgenet.get_interface().name()}"
        self._recv_sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
        self._recv_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._recv_sock.bind((receive_addr, self.DATA_PORT))
        self._recv_sock.listen(3)
        self._recv_poll = select.poll()
        self._recv_poll.register(self._recv_sock, select.POLLIN)

        self.inventory = set(
            name for name, ftype, *_ in os.ilistdir(File.APPS_DIR) if ftype == FTYPE_DIR
        )
        self.inventory -= STOCK_APPS.get(firmware_version(), STOCK_APPS["v1.1.0"])
        self.peer_inventory = None
        self.requested_app = "testeroni"
        self.send_app_loop = None
        self.receive_app_loop = None
        self.finished = False

        # UI init
        self._ui_dots: int = 0
        self._time_since_connect: int = -1
        self._ui_num_bytes: int = -1  # TODO: connect to logic
        self._ui_num_bytes_processed: int = -1  # TODO: connect to logic

        log.info("init complete")

    def think(self, ins: InputState, delta_ms: int) -> None:
        self._time = self._time + delta_ms
        wrapped_time = self._time % 50
        if delta_ms > wrapped_time:
            self.ui_think(ins, delta_ms)

        self.discover()
        self.beacon()
        if not self._peer:
            return

        if self._config.is_sender:
            if not self.send_app_loop:
                self.send_inventory()
                requested = self.check_app_request()
                while not requested:
                    requested = self.check_app_request()
                self.send_app_loop = self.send_app(requested)
            try:
                next(self.send_app_loop)
            except StopIteration as stop:
                if stop.value:
                    log.info(f"sent {stop.value}")
                else:
                    log.info("no app")
                self.send_app_loop = None

        elif not self.finished:
            if not self.receive_app_loop:
                if self.peer_inventory is None:
                    self.peer_inventory = self.receive_inventory()
                if not self.requested_app:
                    return
                elif not self.request_app(self.requested_app):
                    self.requested_app = None
                    return
                self.receive_app_loop = self.receive_app(50)
            try:
                next(self.receive_app_loop)
            except StopIteration as stop:
                if stop.value:
                    self.finished = True
                    log.info(f"got {stop.value}")
                else:
                    log.info("no app")
                self.receive_app_loop = None

    def discover(self):
        for _, _ in self._discover_poll.poll(100):
            peer_beacon, addr = self._discover_sock.recvfrom(self.DISCOVER_BEACON_LEN)
            try:
                version = peer_beacon.decode().split(":")[1][1:].split(".", 3)
                version = tuple(int(v) for v in version)
            except (IndexError, TypeError, ValueError) as err:
                log.error(f"wrong beacon format {err}")
                return
            new_peer = (
                f"{addr[0]}%{badgenet.get_interface().name()}",
                version,
            )
            if new_peer != self._peer:
                self._peer = new_peer
                ver_str = ".".join(str(i) for i in self._peer[1])
                log.info(f"new v{ver_str} peer: {self._peer[0]} ")

    def beacon(self):
        beacon_str = f"Pollin4te:{firmware_version()}"
        self._discover_sock.sendto(
            beacon_str.encode(), (self._local_addr, self.DISCOVER_PORT)
        )

    def send_inventory(self):
        if not self._peer:
            return
        log.info("sending inventory")
        self._send_sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
        self._send_sock.connect((self._peer[0], self.DATA_PORT))
        self._send_sock.send(json.dumps(list(self.inventory)).encode())
        try:
            self.wait_for_sync(f"INVENTORY {len(self.inventory)}")
        except DesyncError:
            log.warning("wrong inventory sync")
            return

    def receive_inventory(self):
        log.info("requesting inventory")
        conn, _ = self._recv_sock.accept()
        inventory_json = conn.recv(INVENTORY_TX_MAX_SIZE).decode()
        log.info(f"received inventory: {inventory_json}")
        inventory = json.loads(inventory_json)
        self.sync_with_sender(conn, f"INVENTORY {len(inventory)}")
        log.info(f"{inventory}")
        return inventory

    def request_app(self, app_name: str) -> bool:
        for _, _ in self._recv_poll.poll(100):
            conn, _ = self._recv_sock.accept()
            _ = conn.recv(10).decode()
            conn.send(app_name.encode())
            log.info(f"app requested: {app_name}")
            result = conn.recv(8).decode()
            conn.close()
            if result != "OK":
                log.warning(f"app request denied for '{app_name}'")
            else:
                return True
        return False

    def check_app_request(self) -> str | None:
        if not self._peer:
            return
        self._send_sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
        self._send_sock.connect((self._peer[0], self.DATA_PORT))
        self._send_sock.send("anything?".encode())
        app_name = self._send_sock.recv(1024).decode()
        if app_name in self.inventory:
            self._send_sock.send("OK".encode())
            return app_name
        else:
            log.warning(f"requested unavailable app {app_name}")
            self._send_sock.send("N/A".encode())

    def send_app(self, app_name: str) -> Generator[None, None, str | None]:
        if not self._peer:
            return
        log.info("sending app")
        to_send = AppPackage(app_name)
        self._ui_num_bytes = sum(file.data_len for file in to_send.files)
        self._send_sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
        try:
            self._send_sock.settimeout(10)
            self._send_sock.connect((self._peer[0], self.DATA_PORT))
            self._send_sock.send(to_send.to_json().encode())
            self.wait_for_sync(f"INFO")
            self._send_sock.settimeout(1000)
            self._ui_num_bytes_processed = 0
            for file in to_send.files:
                file_pos = 0
                for chunk in file.read():
                    self._send_sock.send(chunk)
                    file_pos += len(chunk)
                    self.wait_for_sync(f"CHUNK {file.path} {file_pos}")
                    self._ui_num_bytes_processed += len(chunk)
                    yield
                self.wait_for_sync(f"FILE {file.path}")
            self.wait_for_sync(f"APP {to_send.app_name}")
        except (OSError, DesyncError) as err:
            log.error(str(err))
            self._send_sock.close()
            return None
        self._send_sock.close()
        return to_send.app_name

    def wait_for_sync(self, state: str):
        if self._send_sock.recv(1024).decode() != f"OK {state}":
            raise DesyncError(state)

    def receive_app(self, timeout_ms: int) -> Generator[None, None, AppPackage | None]:
        for _, _ in self._recv_poll.poll(timeout_ms):
            log.info("receiving app")
            conn, addr = self._recv_sock.accept()
            app_json = conn.recv(4096)
            self.sync_with_sender(conn, f"INFO")
            app_info = json.loads(app_json)
            self._ui_num_bytes = sum(file["length"] for file in app_info["files"])
            self._ui_num_bytes_processed = 0
            try:
                os.mkdir(f"{File.APPS_DIR}/{app_info['name']}")
            except OSError:
                pass
            for file in app_info["files"]:
                if file["is_dir"]:
                    try:
                        os.mkdir(file["path"])
                    except OSError:
                        pass
                    time.sleep(0.1)
                else:
                    with open(
                        f"{File.APPS_DIR}/{app_info['name']}/{file['path']}", "wb"
                    ) as f:
                        file_size = file["length"]
                        file_pos = 0
                        while file_pos < file_size:
                            chunk = conn.recv(FILE_TX_CHUNK_SIZE)
                            f.write(chunk)
                            file_pos += len(chunk)
                            self.sync_with_sender(
                                conn, f"CHUNK {file['path']} {file_pos}"
                            )
                            self._ui_num_bytes_processed += len(chunk)
                            yield
                self.sync_with_sender(conn, f"FILE {file['path']}")
            self.sync_with_sender(conn, f"APP {app_info['name']}")
            conn.close()
            return AppPackage.from_json(app_info)
        else:
            return None

    def sync_with_sender(self, conn: socket.socket, state: str):
        log.info(f"syncing with sender: {state}")
        conn.send(f"OK {state}".encode())

    def ui_think(self, ins: InputState, delta_ms: int) -> None:
        self._ui_dots = (self._ui_dots + 1) % 4
        if self._peer:
            self._time_since_connect += 1

    def draw(self, ctx: Context) -> None:
        # fill background
        ctx.rgb(0.1, 0.1, 0.2).rectangle(-120, -120, 240, 240).fill()

        # write text
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE

        ctx.scale(1.0, 1)
        ctx.move_to(0, -80)
        ctx.rgb(*GO_GREEN).text("Pollin4te")

        ctx.rgb(1, 1, 1)
        ctx.scale(0.8, 0.8)
        ctx.move_to(0, -55)
        mode = "sender" if self._config.is_sender else "receiver"
        ctx.text(f"Mode: {mode}")

        if self._time_since_connect < 3:
            self.render_searching(ctx)
        else:
            self.render_loading(ctx)

    def render_searching(self, ctx: Context) -> None:
        ctx.text_align = ctx.LEFT
        ctx.move_to(-120, 0)
        if self._time_since_connect == -1:
            points = "".join(["." for _ in range(self._ui_dots)])
            ctx.text(f"Searching{points}")
        else:
            ctx.text(f"Connected!")

        ctx.move_to(-120, 25)
        peer = self._peer[0][-8:-4] if self._peer else "[n/a]"
        ctx.text(f"Peer: {peer}")

    def render_loading(self, ctx: Context) -> None:
        ctx.text_align = ctx.LEFT
        ctx.move_to(-120, 0)

        mode = "sending" if self._config.is_sender else "receiving"

        points = "".join(["." for _ in range(self._ui_dots)])
        ctx.text(f"{mode}{points}")

        # bar filling
        chunk_size = 240 / self._ui_num_bytes
        filling = chunk_size * self._ui_num_bytes_processed
        if filling > 0:
            ctx.rgb(*GO_GREEN).rectangle(-120, 15, filling, 20).fill()
        # bar border
        ctx.rgb(*self.PURPLE).rectangle(-120, 15, 240, 20).stroke()

        # done
        if self._ui_num_bytes == self._ui_num_bytes_processed:
            ctx.text_align = ctx.CENTER
            ctx.move_to(0, 60)
            ctx.rgb(1, 1, 1).text("Done!")

            ctx.move_to(0, 60)
            ctx.scale(2, 2)
            ctx.font = "Material Icons"
            ctx.rgb(*self.PURPLE).text("\uea1c")
            ctx.restore()


### debugging foo:


def write_test_app():
    app_files = [
        Dir("testeroni", "/"),
        File(
            "testeroni",
            "/flow3r.toml",
            """\
[app]
name = "Testeroni"
menu = "Apps"

[entry]
class = "App"

[metadata]
author = "test"
license = "none"
url = "https://example.org"
""".encode(),
        ),
        File(
            "testeroni",
            "/__init__.py",
            """\
from st3m.application import Application
class App(Application):
    def __init__(self, app_ctx):
        super().__init__(app_ctx)
        # print("Testeroni App")
    def think(self, ins, delta_ms):
        pass
    def draw(self, ctx):
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        ctx.move_to(0, 0)
        ctx.scale(1.0, 1)
        ctx.rgb(1, 1, 1).text("Testeroni")
""".encode(),
        ),
    ]

    for file in app_files:
        file.write()


import st3m.run

st3m.run.run_view(Pollin4teApp(ApplicationContext()))
